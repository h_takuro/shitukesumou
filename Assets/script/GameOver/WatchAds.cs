﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WatchAds : MonoBehaviour {

	public Text watchAdsText;
	public Button button;
	// Use this for initialization
	void Start () {
		watchAdsText.text = (Status.Instance.highScore * 5).ToString() + " KP\nGET!!!";
		if(Status.Instance.gameCount % 5 != 0 || Status.Instance.adFlg == false) {
			watchAdsText.gameObject.SetActive (false);
			button.gameObject.SetActive(false);
			Status.Instance.adFlg = true;
		}
	}

	// Update is called once per frame
	void Update () {
		if(Status.Instance.adFlg == false) {
			watchAdsText.gameObject.SetActive (false);
			button.gameObject.SetActive(false);
		}
	}
}
