using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdsScript : MonoBehaviour {

  void Awake()
  {
		if (Application.platform == RuntimePlatform.Android) {
        //端末がAndroidだった場合の処理
				Advertisement.Initialize ("1748954");
    } else if (Application.platform == RuntimePlatform.IPhonePlayer) {
        //端末がiOSだった場合の処理
	      Advertisement.Initialize ("1748953");
    }

  }

  public void ShowAd() {
		if (Advertisement.IsReady("rewardedVideo")) {
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);
			Status.Instance.adFlg = false;
		}
  }

	private void HandleShowResult(ShowResult result) {
		switch (result) {
			case ShowResult.Finished:
				Debug.Log ("The ad was successfully shown.");
				Status.Instance.totalScore += Status.Instance.highScore * 5;
				break;
			case ShowResult.Skipped:
				Debug.Log("The ad was skipped before reaching the end.");
				break;
			case ShowResult.Failed:
				Debug.LogError("The ad failed to be shown.");
				break;
		}
	}
}
