using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalPoint : MonoBehaviour {

	public Text totalPointText;
	private string totalScoreKey = "TOTAL SCORE";
	private string totalScoreKey2 = "TOTAL SCORE2";
	private string gameCountKey = "GAME COUNT";

	// Use this for initialization
	void Start () {
		Status.Instance.totalScore += Status.Instance.score;
		totalPointText.text = "TOTAL\n" + Status.Instance.totalScore.ToString() + " KP";
		
	}

	// Update is called once per frame
	void Update () {
		totalPointText.text = "TOTAL\n" + Status.Instance.totalScore.ToString() + " KP";
		Collection.SetLong(totalScoreKey, totalScoreKey2, Status.Instance.totalScore);
	}
}
