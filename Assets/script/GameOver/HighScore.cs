﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

	public Text highScoreText;
	private string key = "HIGH SCORE";
	private string key2 = "HIGH SCORE2";

	// Use this for initialization
	void Start () {
		if (Status.Instance.score > Status.Instance.highScore) {
	    Status.Instance.highScore = Status.Instance.score;
			Collection.SetLong(key, key2, Status.Instance.highScore);
			highScoreText.text = "new!! HighScore\n" + Status.Instance.highScore.ToString() + " KP";
    } else {
			highScoreText.text = "HighScore\n" + Status.Instance.highScore.ToString() + " KP";
		}
	}

	// Update is called once per frame
	void Update () {
	}
}
