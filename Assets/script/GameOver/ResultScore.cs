﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScore : MonoBehaviour {

	public Text score;
	// Use this for initialization
	void Start () {
		if(Status.Instance.score == 0) {
			score.text = "";
		} else {
			score.text = Status.Instance.score.ToString() + " KP GET!";
		}

	}

	// Update is called once per frame
	void Update () {

	}
}
