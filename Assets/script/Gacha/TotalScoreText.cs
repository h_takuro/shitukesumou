﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalScoreText : MonoBehaviour {

	public Text totalScoreText;
	private string Key = "TOTAL SCORE";
	// Use this for initialization
	void Start () {
		totalScoreDisplay();
	}

	// Update is called once per frame
	void Update () {

	}

	public void onClick() {
		totalScoreDisplay();
	}

	private void totalScoreDisplay() {
		if(Status.Instance.totalScore > Status.Instance.gachaKP) {
			totalScoreText.text = Status.Instance.totalScore + "KP → " + (Status.Instance.totalScore - Status.Instance.gachaKP) + "KP";
		} else {
			totalScoreText.text = Status.Instance.totalScore + "KP";
		}
	}
}
