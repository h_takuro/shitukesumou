﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GetWeaponImage : MonoBehaviour {

	[SerializeField]
	private Sprite[] sprite;
	[SerializeField]
	private Sprite[] spriteRare;
	[SerializeField]
	private Sprite noMoneySprite;
	[SerializeField]
	private Sprite nonSprite;
	[SerializeField]
	private Image image;
	[SerializeField]
	private Image imageRare;


	private string totalScoreKey = "TOTAL SCORE";
	private string totalScoreKey2 = "TOTAL SCORE2";
	private string weaponKey = "WEAPON";
	private string haveWeaponKey = "HAVE WEAPON";
	private string weaponLevelKey = "WEAPON LEVEL";
	private string gachaCountKey = "GACHA COUNT";
	private string weaponName;
	private int weaponInt;
	private string haveWeaponCsv;
	private string weaponLevelCsv;

	void Start () {

	}

	public void OnClick() {
		int rand = UnityEngine.Random.Range(0, 10000);
		if(Status.Instance.totalScore >= Status.Instance.gachaKP) {
			switch(Status.Instance.gachaRank) {
				case 0:
				if(rand > 9998) {
					weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				} else if(rand > 9700) {
					weaponInt = (int)Weapon.WeaponType.pikopiko_hammer;
				} else if(rand > 9000) {
					weaponInt = (int)Weapon.WeaponType.gom_hammer;
				} else {
					weaponInt = (int)Weapon.WeaponType.bin;
				}
				break;

				case 1:
				if(rand > 9998) {
					weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				} else if(rand > 9300) {
					weaponInt = (int)Weapon.WeaponType.pikopiko_hammer;
				} else if(rand > 600) {
					weaponInt = (int)Weapon.WeaponType.gom_hammer;
				} else {
					weaponInt = (int)Weapon.WeaponType.bin;
				}
				break;

				case 2:
				if(rand > 9900) {
					weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				} else if(rand > 1100) {
					weaponInt = (int)Weapon.WeaponType.pikopiko_hammer;
				} else if(rand > 550) {
					weaponInt = (int)Weapon.WeaponType.gom_hammer;
				} else {
					weaponInt = (int)Weapon.WeaponType.bin;
				}
				break;

				case 3:
				if(rand > 9100) {
					weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				} else if(rand > 5400) {
					weaponInt = (int)Weapon.WeaponType.pikopiko_hammer;
				} else if(rand > 2000) {
					weaponInt = (int)Weapon.WeaponType.gom_hammer;
				} else {
					weaponInt = (int)Weapon.WeaponType.bin;
				}
				break;

				case 4:
				if(rand > 4000) {
					weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				} else if(rand > 2700) {
					weaponInt = (int)Weapon.WeaponType.pikopiko_hammer;
				} else if(rand > 1300) {
					weaponInt = (int)Weapon.WeaponType.gom_hammer;
				} else {
					weaponInt = (int)Weapon.WeaponType.bin;
				}
				break;

				default:
				weaponInt = (int)Weapon.WeaponType.karaoke_rimokon;
				break;
			}

			if(!Status.Instance.haveWeapon.Contains(weaponInt)) {
				Status.Instance.haveWeapon.Add(weaponInt);
			} else {
				Status.Instance.weaponLevel[weaponInt] ++;
			}

			Status.Instance.gachaCount ++;

			image.GetComponent<Image>().sprite = sprite[weaponInt];
			imageRare.GetComponent<Image>().sprite = spriteRare[weaponInt];
			Status.Instance.totalScore -= Status.Instance.gachaKP;

			haveWeaponCsv = Collection.ListToCsv(Status.Instance.haveWeapon);
			weaponLevelCsv = Collection.ListToCsv(Status.Instance.weaponLevel);

			Collection.SetLong(totalScoreKey, totalScoreKey2, Status.Instance.totalScore);
			PlayerPrefs.SetString(haveWeaponKey, haveWeaponCsv);
			PlayerPrefs.SetString(weaponLevelKey, weaponLevelCsv);
			PlayerPrefs.SetInt(gachaCountKey, Status.Instance.gachaCount);
			PlayerPrefs.Save();

			if(Status.Instance.gachaCount == Status.Instance.rankLimit) {
				Status.Instance.gachaCount = 0;

				PlayerPrefs.SetInt("GACHA COUNT", Status.Instance.gachaCount);
				SceneManager.LoadScene ("Reincarnation");
			}

		} else {
			image.GetComponent<Image>().sprite = noMoneySprite;
			imageRare.GetComponent<Image>().sprite = nonSprite;
		}
	}
}
