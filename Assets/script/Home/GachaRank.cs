﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GachaRank : MonoBehaviour {

	public Text text;
	// Use this for initialization
	void Start () {
		text.text = "RANK\n" + (Status.Instance.gachaRank + 1).ToString();
	}

	// Update is called once per frame
	void Update () {

	}
}
