﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GachaButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(Status.Instance.initFlg == 1) {
			SceneManager.LoadScene ("YourName");
		}

	}

	// Update is called once per frame
	void Update () {

	}

	public void OnClick() {
		SceneManager.LoadScene ("GachaGacha");
	}
}
