﻿using System.Collections;
 using System.Collections.Generic; 
using UnityEngine;
using UnityEngine.SceneManagement;

  public class EnemyController : MonoBehaviour {  
  private Rigidbody2D rb; 
	private GameObject _child;

  private SpriteRenderer MainSpriteRenderer;
  public Sprite sprite0;
  public Sprite sprite1;
   public Sprite sprite2;

  private string weaponLevelKey = "WEAPON LEVEL";
  private string weaponLevelCsv;
  public float speedX;
  public float speedY; 
  public int life; 
  public int status;

  void Start() 	{ 
    rb = GetComponent<Rigidbody2D> (); 
		_child = transform.Find ("hit").gameObject;
 		 _child.SetActive (false);
    MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer> ();  
  }  

  void FixedUpdate()  { 
    rb.velocity = new Vector2 (speedX, speedY); 
  }

  void OnMouseDown() 	{
    this.transform.position += new Vector3 (0, -0.3f, 0); 
    StartCoroutine ("DelayMethod", 0.07f);
    _child.SetActive (true); 
    Attacked();
  }  

  private IEnumerator DelayMethod(float waitTime) 	{ 
    yield return new WaitForSeconds(waitTime); 
    this.transform.position += new Vector3 (0, 0.3f, 0); 
    _child.SetActive (false); 
  }  

  private void Attacked() {
    int attack;
    switch(Status.Instance.weapon) {
      case (int)Weapon.WeaponType.bin:
        attack = 10 + Status.Instance.weaponLevel[Status.Instance.weapon];
        AddScore(attack);
        Damage(1);
        Injury(1);
        Dead(0);
        break;

      case (int)Weapon.WeaponType.gom_hammer:
        attack = 10 + Status.Instance.weaponLevel[Status.Instance.weapon];
        AddScore(attack);
        Damage(1);
        Injury(1);
        if(status == 2) {
          Dead(-1);
        } else {
          Dead(0);
        }
        break;

      case (int)Weapon.WeaponType.pikopiko_hammer:
        attack = (10 + Status.Instance.weaponLevel[Status.Instance.weapon])/2;
        AddScore(attack);
        Damage(1);
        if(status == 2) {
          Injury(1);
          Dead(0);
        }
        break;

      case (int)Weapon.WeaponType.karaoke_rimokon:
        attack = Status.Instance.weaponLevel[(int)Weapon.WeaponType.bin] * (2 + Status.Instance.weaponLevel[Status.Instance.weapon]);
        AddScore(attack);
        Damage(1);
        Injury(1);
        Dead(0);
        break;

      default:
        break;
    }
  }

  private void AddScore(int n) {
    Status.Instance.score += n; 
  }

  private void Damage(int n) {
    life -= n;
  }

  private void Injury(int n) {
    if(life == n) {
      MainSpriteRenderer.sprite = sprite1; 
    }
  }

  private void Dead(int n) {
    if(life == n) {
      MainSpriteRenderer.sprite = sprite2;
      speedX = 0;
      speedY = -10;
    }
  }
}  
