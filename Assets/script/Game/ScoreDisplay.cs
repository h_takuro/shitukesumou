﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {

	// スコアを表示する
	public Text scoreText;

	void Start ()
	{
		Status.Instance.score = 0;
	}

	void Update ()
	{
		// スコアを表示する
		scoreText.text = Status.Instance.score.ToString ();
	}
}
