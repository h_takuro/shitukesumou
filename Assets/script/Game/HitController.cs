﻿using System.Collections; 
using System.Collections.Generic;
 using UnityEngine;  

public class HitController : MonoBehaviour {   

  // Use this for initialization
  void Start () { 
  }  

  // Update is called once per frame
  void Update () {  
  }  

  public void OnMouseDown(){ 
    StartCoroutine ("DelayMethod", 1f);
  }  

  private IEnumerator DelayMethod(float waitTime) { 
    yield return new WaitForSeconds(waitTime); 
  }
 }  
