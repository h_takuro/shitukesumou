﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

	private SpriteRenderer spriteRenderer;
	public Sprite sprite0;
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	private string key = "WEAPON";

	// Use this for initialization
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();  
		switch(Status.Instance.weapon) {
			case 0:
				spriteRenderer.sprite = sprite0;
				break;
			case 1:
				spriteRenderer.sprite = sprite1;
				break;
			case 2:
				spriteRenderer.sprite = sprite2;
				break;
			case 3:
				spriteRenderer.sprite = sprite3;
				break;
			default:
				break;
		}
	}

	// Update is called once per frame
	void Update () {
	}

	public void OnClickedEnemy () {
		transform.position += new Vector3 (0.8f, -0.3f, 0);
		transform.Rotate (new Vector3 (0, 0, -60));
		StartCoroutine ("DelayMethod", 0.13f);
	}

	private IEnumerator DelayMethod(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		transform.position += new Vector3 (-0.8f, 0.3f, 0);
		transform.Rotate (new Vector3 (0, 0, 60));
	}
}
