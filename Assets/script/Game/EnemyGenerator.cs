﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyGenerator : MonoBehaviour {

	//生まれてくる敵プレハブ
	public GameObject[] enemyPrefab;
	GameObject enemyObject;
	EnemyController enemy;
	private float speed = -4.0f;
	private bool generate = true;
	private string gameCountKey = "GAME COUNT";

	// Use this for initialization
	void Start () {
		Status.Instance.gameCount ++;
		PlayerPrefs.SetInt(gameCountKey, Status.Instance.gameCount);
	}

	void Update() {
		Generate ();
		Check ();
		Delete ();
	}

	void Generate() {
		if (enemyObject == null && generate == true) {
			enemyObject = Instantiate (enemyPrefab[Random.Range(0, enemyPrefab.Length)], new Vector3 (5.0f, -1.67f, 0), transform.rotation);
			enemy = enemyObject.GetComponent<EnemyController>();
			enemyObject.SetActive (true);
			enemy.speedX = speed;
			speed += -0.4f;
		}
	}

	void Check() {
		if(enemyObject.transform.position.y < -10.0f) {
      GameOver();
    }
    if(enemyObject.transform.position.x < -6.0f && enemy.life > 1) {
      GameOver();
    }
	}

	void GameOver() {
		generate = false;
		StartCoroutine ("DelayMethod", 1.5f);
	}

	void Delete() {
		if(enemyObject.transform.position.x < -7.0f) {
			Destroy (enemyObject);
		}
	}

	private IEnumerator DelayMethod(float waitTime) 	{ 
    yield return new WaitForSeconds(waitTime); 
		SceneManager.LoadScene ("GameOver");
  }  
}
