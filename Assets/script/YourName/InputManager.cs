using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour {

    InputField inputField;
		string key = "YOUR NAME";
		string initKey = "INIT";

    /// <summary>
    /// Startメソッド
    /// InputFieldコンポーネントの取得および初期化メソッドの実行
    /// </summary>
    void Start() {

        inputField = GetComponent<InputField>();
    }



    /// <summary>
    /// Log出力用メソッド
    /// 入力値を取得してLogに出力し、初期化
    /// </summary>


    public void InputLogger() {

      string inputValue = inputField.text;
 			Status.Instance.yourName = inputValue;
			PlayerPrefs.SetString(key, Status.Instance.yourName);
			Status.Instance.initFlg = 0;
			PlayerPrefs.SetInt(initKey, Status.Instance.initFlg);
			SceneManager.LoadScene ("home");

    }


}
