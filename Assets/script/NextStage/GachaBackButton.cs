using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GachaBackButton : MonoBehaviour {

	private string gachaRankKey = "GACHA RANK";
	private string gachaKpKey = "GACHA KP";
	private string rankLimitKey = "RANK LIMIT";

	// Use this for initialization
	void Start () {
		Status.Instance.gachaRank ++;
		Status.Instance.gachaKP *= 2;
		Status.Instance.rankLimit *= 10;
		PlayerPrefs.SetInt(gachaRankKey, Status.Instance.gachaRank);
		PlayerPrefs.SetInt(gachaKpKey, Status.Instance.gachaKP);
		PlayerPrefs.SetInt(rankLimitKey, Status.Instance.rankLimit);
		PlayerPrefs.Save();
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnClick() {
		SceneManager.LoadScene ("GachaGacha");
	}
}
