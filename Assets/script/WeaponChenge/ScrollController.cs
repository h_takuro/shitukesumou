using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScrollController : MonoBehaviour {

	[SerializeField]
	private GameObject btnPref;  //ボタンプレハブ
	//ボタン表示数
	[SerializeField]
	private Sprite[] sprite;
	[SerializeField]
	private Sprite spriteNone;
	const int BUTTON_COUNT = 4;
	private string key = "WEAPON";
	private string haveWeaponKey = "HAVE WEAPON";
	private GameObject _child;

	void Start () {
		//Content取得(ボタンを並べる場所)
		RectTransform content = GameObject.Find("Canvas/ScrollView/Content").GetComponent<RectTransform>();

		//Contentの高さ決定
		//(ボタンの高さ+ボタン同士の間隔)*ボタン数
		float btnSpace = content.GetComponent<VerticalLayoutGroup>().spacing;
		float btnHeight = btnPref.GetComponent<LayoutElement>().preferredHeight;
		content.sizeDelta = new Vector2(0, (btnHeight + btnSpace) * BUTTON_COUNT);
		for (int i = 0; i < BUTTON_COUNT; i++) {
			int no = i;
			//ボタン生成
			GameObject btn = (GameObject)Instantiate(btnPref);
			//ボタンをContentの子に設定
			btn.transform.SetParent(content, false);
			//ボタンの画像変更
			btn.transform.GetComponent<Image>().sprite = sprite[i];
			//ボタンの色変更
			if(!Status.Instance.haveWeapon.Contains(no)) {
				btn.GetComponent<Button>().image.color = Color.black;
			}

			if(Status.Instance.weapon == no) {
				//btn.GetComponent<Button>().image.color = new Color(1f, 1f, 1f, 0.5f);
			}
			//ボタンのクリックイベント登録
			btn.transform.GetComponent<Button>().onClick.AddListener(() => OnClick(no));
			_child = btn.transform.FindChild ("Text").gameObject;
			_child.GetComponent<Text>().text = Status.Instance.weaponLevel[i].ToString();
		}
	}

	public void OnClick(int no) {
		if(Status.Instance.haveWeapon.Contains(no)) {
			Status.Instance.weapon = no;
			PlayerPrefs.SetInt(key, Status.Instance.weapon);
			SceneManager.LoadScene ("Home");
		}
	}
}
