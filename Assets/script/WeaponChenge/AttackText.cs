using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackText : MonoBehaviour {

	public Text totalPointText;

	// Use this for initialization
	void Start () {
		totalPointText.text = Status.Instance.weaponLevel[0].ToString();
	}
}
