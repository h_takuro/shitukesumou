﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Collection {

	static public int[] CsvToList(string csv) {
		return csv.Split(',').Select(x => int.Parse(x)).ToArray();
	}

	static public string ListToCsv(List<int> list) {
		string haveWeaponCsv = "";
		bool first = true;
		foreach(var i in list) {
			if(first) {
				first = false;
				haveWeaponCsv = i + "";
			} else {
				haveWeaponCsv += "," + i;
			}
		}
		return haveWeaponCsv;
	}

	static public void SetLong(string key1, string key2, long lng) {
		int int1;
		int int2;
		string lngStr = string.Format("{0:0000000000000000000}", lng);
		int1 = int.Parse(lngStr.Substring(10, 9));
		int2 = int.Parse(lngStr.Substring(0, 10));
		PlayerPrefs.SetInt(key1, int1);
		PlayerPrefs.SetInt(key2, int2);
	}
}
