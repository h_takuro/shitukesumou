﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status {
	public readonly static Status Instance = new Status();
	public int level = 0;
	public int weapon = 0;
	public long score = 0;
	public long highScore = 0;
	public long totalScore = 0;
	public int gameCount = 0;
	public int gachaCount = 0;
	public int gachaRank = 0;
	public int gachaKP = 500;
	public int rankLimit = 20;
	public bool adFlg = true;
	public string yourName = "";
	public int initFlg = 1;
	public string UID = "";
	public List<int> haveWeapon = new List<int>();
	public List<int> weaponLevel = new List<int>();
}
