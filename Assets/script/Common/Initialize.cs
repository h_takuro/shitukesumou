﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize {

	[RuntimeInitializeOnLoadMethod()]
	static void Init()
	{
		int highScore;
		int highScore2;
		int totalScore;
		int totalScore2;

		string haveWeaponCsv = PlayerPrefs.GetString("HAVE WEAPON", "0");
		string weaponLevelCsv = PlayerPrefs.GetString("WEAPON LEVEL", "0,0,0,0");
		Status.Instance.weapon = PlayerPrefs.GetInt("WEAPON", 0);

		highScore = PlayerPrefs.GetInt("HIGH SCORE", 0);
		highScore2 = PlayerPrefs.GetInt("HIGH SCORE2", 0);
		Status.Instance.highScore = long.Parse(highScore2.ToString() + highScore.ToString());
		totalScore = PlayerPrefs.GetInt("TOTAL SCORE", 0);
		totalScore2 = PlayerPrefs.GetInt("TOTAL SCORE2", 0);
		if(totalScore < 0) {
			totalScore = 2147483647;
			Status.Instance.totalScore = 2147483647;
		}
		Status.Instance.totalScore = long.Parse(totalScore2.ToString() + totalScore.ToString());
		Status.Instance.gachaRank = PlayerPrefs.GetInt("GACHA RANK", 0);
		Status.Instance.gachaKP = PlayerPrefs.GetInt("GACHA KP", Status.Instance.gachaKP);
		Status.Instance.gachaCount = PlayerPrefs.GetInt("GACHA COUNT", 0);
		Status.Instance.gameCount = PlayerPrefs.GetInt("GAME COUNT", 0);
		Status.Instance.rankLimit = PlayerPrefs.GetInt("RANK LIMIT", Status.Instance.rankLimit);
		Status.Instance.yourName = PlayerPrefs.GetString("YOUR NAME", "");
		Status.Instance.initFlg = PlayerPrefs.GetInt("INIT", 1);
		if(Status.Instance.initFlg == 1) {
			System.Guid guid=System.Guid.NewGuid();
			string _uuid=guid.ToString();
			PlayerPrefs.SetString("UID", _uuid);
		}
		Status.Instance.UID = PlayerPrefs.GetString("UID", "");

		Status.Instance.haveWeapon = new List<int>(Collection.CsvToList(haveWeaponCsv));
		Status.Instance.weaponLevel = new List<int>(Collection.CsvToList(weaponLevelCsv));
	}
}
