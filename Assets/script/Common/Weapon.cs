﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Weapon {

	public enum WeaponType {
		bin,
		gom_hammer,
		pikopiko_hammer,
		karaoke_rimokon
	};

	static public string GetWeaponName(int i) {
		switch(i) {
			case (int)WeaponType.bin: return "BIN";
			case (int)WeaponType.gom_hammer: return "GOM HAMMER";
			case (int)WeaponType.pikopiko_hammer: return "PIKOPIKO HAMMER";
			case (int)WeaponType.karaoke_rimokon: return "KARAOKE RIMOKON";
			default: return "";
		}
	}

	static public string GetWeaponRare(int i) {
		switch(i) {
			case (int)WeaponType.bin: return "N";
			case (int)WeaponType.gom_hammer: return "R";
			case (int)WeaponType.pikopiko_hammer: return "SR";
			case (int)WeaponType.karaoke_rimokon: return "SSR";
			default: return "";
		}
	}
}
